import logging
import json


class SmsLogHandler(logging.Handler):

    def __init__(self):
        super(SmsLogHandler, self).__init__()

    def emit(self, log_record):
        self.__write_log_entry(
            self.__prepare_row(log_record)
        )

    def __prepare_row(self, log_record):
        result = log_record.msg.copy()
        result['log_level'] = log_record.levelname
        result['gate_response'] = json.dumps(result.get('gate_response', {}))
        return result

    def __write_log_entry(self, row):
        from .models import SmsLog
        SmsLog.objects.create(**row)
