from __future__ import unicode_literals

from django.apps import AppConfig
from sms_handler.handler_registry import HandlerRegistry


class SmsConfig(AppConfig):
    name = 'sms'

    def ready(self):
    	super(SmsConfig, self).ready()

    	HandlerRegistry.registrate('sms.sms_handlers.SuperSmsGateHandler')



