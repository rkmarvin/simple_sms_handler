from __future__ import unicode_literals

from django.db import models
from sms_handler.base_sms_handler import STATUS_ERROR, STATUS_OK


STATUS_AWAITING = 'awaiting'


class SmsLog(models.Model):

    STATUSES = (
        (STATUS_OK, 'ok'),
        (STATUS_ERROR, 'error'),
        (STATUS_AWAITING, 'awaiting'),
    )

    phone = models.CharField(max_length=11, verbose_name='Phone')
    status = models.CharField(max_length=8, choices=STATUSES, 
                              verbose_name='Status', default=STATUS_AWAITING)
    message = models.CharField(max_length=150, verbose_name='SMS Message')
    gate_response = models.TextField(verbose_name='Gate response data', default='{}')
    log_level = models.CharField(max_length=10, verbose_name='Log level')

    def __unicode__(self):
        return u'%s %s' % (self.phone, self.status)

    class Meta:
        verbose_name = 'SMS Log'
        verbose_name_plural = 'SMS Logs'