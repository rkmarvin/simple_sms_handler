#! coding: utf-8
import responses
import json

from django.test import TestCase
from sms_handler.handler_registry import get_handler
from sms_handler import base_sms_handler
from .models import SmsLog


class BaseTestCaseMixIn(object):
    PHONE = '79149009900'
    OK_RESPONSE = {'status': 'ok', 'phone': PHONE}
    ERROR_RESPONSE = {
        'status': 'error',
        'phone': PHONE,
        'error_code': -3500,
        'error_msg': u'Невозможно отправить сообщение указанному абоненту'
    }
    HANDLER_RESPONSE_OK = {
        'phone': PHONE,
        'status': base_sms_handler.STATUS_OK,
        'message': 'message',
        'gate_response': OK_RESPONSE
    }
    HANDLER_RESPONSE_ERROR = {
        'phone': PHONE,
        'status': base_sms_handler.STATUS_ERROR,
        'message': 'message',
        'gate_response': ERROR_RESPONSE
    }
    AUTH_CRED = {'api-key': 'some_api_key'}


class TestSuperSmsGateHandler(BaseTestCaseMixIn, TestCase):

    def setUp(self):
        self.handler = get_handler('sms.sms_handlers.SuperSmsGateHandler', **self.AUTH_CRED)

    @responses.activate
    def test_when_sendign_sms_with_ok_status_then_logged_it(self):
        self.assertEquals(SmsLog.objects.all().count(), 0)

        responses.add(responses.POST, self.handler.API_URL,
                      body = json.dumps(self.OK_RESPONSE),
                      content_type='application/json',
                      status=200)
        resp = self.handler.send('message', self.PHONE)

        self.assertEquals(SmsLog.objects.all().count(), 1)
        self.assertEquals(SmsLog.objects.get().phone, self.PHONE)
        self.assertEquals(SmsLog.objects.get().log_level, 'INFO')
        self.assertEquals(SmsLog.objects.get().status, base_sms_handler.STATUS_OK)

    @responses.activate
    def test_when_sendign_sms_with_error_status_then_logged_it(self):
        self.assertEquals(SmsLog.objects.all().count(), 0)

        responses.add(responses.POST, self.handler.API_URL,
                      body = json.dumps(self.ERROR_RESPONSE),
                      content_type='application/json',
                      status=400)
        resp = self.handler.send('message', self.PHONE)

        self.assertEquals(SmsLog.objects.all().count(), 1)
        self.assertEquals(SmsLog.objects.get().phone, self.PHONE)
        self.assertEquals(SmsLog.objects.get().log_level, 'INFO')
        self.assertEquals(SmsLog.objects.get().status, base_sms_handler.STATUS_ERROR)

    @responses.activate
    def test_when_sendign_sms_with_critical_error_status_then_logged_it(self):
        self.assertEquals(SmsLog.objects.all().count(), 0)

        responses.add(responses.POST, self.handler.API_URL,
                      body = json.dumps(self.OK_RESPONSE),
                      content_type='text/json',
                      status=200)
        resp = self.handler.send('message', self.PHONE)

        self.assertEquals(SmsLog.objects.all().count(), 1)
        self.assertEquals(SmsLog.objects.get().phone, self.PHONE)
        self.assertEquals(SmsLog.objects.get().log_level, 'CRITICAL')
        self.assertEquals(SmsLog.objects.get().status, base_sms_handler.STATUS_ERROR)
