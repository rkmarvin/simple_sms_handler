from django.conf import settings

from sms_handler.base_sms_handler import BaseSmsHandler


class SuperSmsGateHandler(BaseSmsHandler):
	API_URL = 'http://super.sms.gate.ru/superapi/message'

	def prepare_post_data(self, **request):
		return {
			'message': request.get('message'),
			'phone': request.get('phone'),
			'api-key': getattr(settings, 'SUPER_SMS_GATE_API_KEY', None)
		}
