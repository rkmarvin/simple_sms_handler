# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-04 07:10
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    initial = True

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='SmsLog',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('phone', models.CharField(max_length=11, verbose_name='Phone')),
                ('status', models.CharField(choices=[(b'ok', 'ok'), (b'error', 'error'), ('awaiting', 'awaiting')], default='awaiting', max_length=8, verbose_name='Status')),
                ('messate', models.CharField(max_length=150, verbose_name='SMS Message')),
                ('gate_response', models.TextField(default='{}', verbose_name='Gate response data')),
                ('log_level', models.PositiveIntegerField(default=10, verbose_name='log_level')),
            ],
            options={
                'verbose_name': 'SMS Log',
                'verbose_name_plural': 'SMS Logs',
            },
        ),
    ]
