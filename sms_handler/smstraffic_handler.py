#! coding: utf-8
from base_sms_handler import BaseSmsHandler


class SMSTrafficHandler(BaseSmsHandler):
	API_URL = 'http://smstraffic.ru/superapi/message'

	def prepare_post_data(self, **request):
		return {
			'message': request.get('message'),
			'phone': request.get('phone'),
			'password': self.auth_cred['password'],
			'login': self.auth_cred['login'],
		}
