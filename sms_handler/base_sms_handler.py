#! coding: utf-8
import requests
import json

from base_logging import BaseLogging



STATUS_OK = 'ok'
STATUS_ERROR = 'error'


class BaseSmsHandler(object):
    API_URL = ''

    def __init__(self, auth_cred, log=None):
        self.auth_cred = auth_cred
        self.log = log if log else BaseLogging()

    def send(self, message, phone):
        request = {'message': message, 'phone': phone}
        response = requests.post(
            self.API_URL,
            params=self.prepare_post_data(**request),
            headers=self.prepare_headers_data()
        )
        return self.process_response(response, **request)

    def prepare_post_data(self, **request):
        raise NotImplementedError

    def prepare_headers_data(self):
        return {'content-type': 'application/json'}

    def process_response(self, response, **request):
        result = {
            'phone': request.get('phone'),
            'message': request.get('message'),
            'status': STATUS_OK if response.status_code == 200 else STATUS_ERROR,
            'gate_response': None
        }

        if not self.__is_application_json(response):
            result['gate_response'] = {'detail': 'Unexpected content type.'} 
            result['status'] = STATUS_ERROR
            self.log.critical(result)
        else:
            result['gate_response'] = response.json()
            self.log.info(result)
        return result

    def __is_application_json(self, response):
        return response.headers.get('Content-Type') in ('application/json',)

