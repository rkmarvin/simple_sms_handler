#! coding: utf-8
import importlib
from base_sms_handler import BaseSmsHandler


class HandlerRegistryException(Exception):
    pass


class HandlerRegistry(object):

    _registrated_objects = {}

    @classmethod
    def registrate(cls, handler_name):
        module_name, class_name = handler_name.rsplit('.', 1)
        module = importlib.import_module(module_name)
        handler_class = getattr(module, class_name, None)

        if not issubclass(handler_class, BaseSmsHandler):
            raise TypeError('Handler `%s` must be instanced of `%s`.' % 
                (handler_class.__name__, BaseSmsHandler.__name__)
            )

        cls._registrated_objects[handler_name] = handler_class

    @classmethod
    def get_handler(cls, handler_name, log=None, **creds):
        if handler_name not in cls._registrated_objects:
            raise HandlerRegistryException(
                    '`%s` not registred.' % handler_name
                ) 
        return cls._registrated_objects[handler_name](creds, log)

    @classmethod
    def clean(cls):
        cls._registrated_objects = {}


get_handler = HandlerRegistry.get_handler