#! coding: utf-8
from base_sms_handler import BaseSmsHandler


class SmscHandler(BaseSmsHandler):
	API_URL = 'http://smsc.ru/someapi/message/'

	def prepare_post_data(self, **request):
		return {
			'mes': request.get('message'),
			'phone': request.get('phone'),
			'pws': self.auth_cred['password'],
			'login': self.auth_cred['login'],
		}
