#! coding: utf-8

import logging 


logger = logging.getLogger('sms_handler.base_sms_log')


class BaseLogging(object):

	def info(self, message):
		logger.info(message)

	def error(self, message):
		logger.error(message)

	def critical(self, message):
		logger.critical(message)