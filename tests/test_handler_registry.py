#! coding: utf-8
from unittest import TestCase
from sms_handler.base_sms_handler import BaseSmsHandler
from sms_handler.handler_registry import HandlerRegistry
from sms_handler.handler_registry import HandlerRegistryException
from sms_handler.handler_registry import get_handler


class MySmsHandler(BaseSmsHandler):
	pass


class TestHandleRegistry(TestCase):

	def setUp(self):
		self.name = 'tests.test_handler_registry.MySmsHandler'
		self.wrong_class_name = 'tests.test_handler_registry.TestHandleRegistry'
		HandlerRegistry.clean()

	def test_registrate_sms_handl(self):
		self.__registrate_handler()
		self.assertTrue(self.name in HandlerRegistry._registrated_objects)
		handler_class = HandlerRegistry._registrated_objects[self.name]
		self.assertEquals(handler_class, MySmsHandler)

	def test_registrate_raised_TypeError_if_handler_not_instance_of_BaseSmsHandler(self):
		with self.assertRaises(TypeError):
			self.__registrate_handler(self.wrong_class_name)

	def test_get_handler(self):
		self.__registrate_handler()
		cred = {'login': 'login', 'passw': 'pass'}
		handler = HandlerRegistry.get_handler(self.name, **cred)
		self.assertEquals(handler.auth_cred, cred)

	def test_get_handler_raised_HandlerRegistrateExpection_if_handler_is_not_registred(self):
		with self.assertRaises(HandlerRegistryException):
			HandlerRegistry.get_handler(self.name)	

	def __registrate_handler(self, name=None):
		name = name if name else self.name
		HandlerRegistry.registrate(name)

	def test_get_handler_as_function(self):
		self.__registrate_handler(self.name)
		cred = {'login': 'login', 'passw': 'pass'}
		handler = get_handler(self.name, **cred)
		self.assertEquals(handler.auth_cred, cred)
