#! coding: utf-8
import json
import responses

from sms_handler.smstraffic_handler import SMSTrafficHandler
from base_test_case import BaseTestCase


class TestSMSTrafficHandler(BaseTestCase):

    def setUp(self):
        self.handler = SMSTrafficHandler(self.AUTH_CRED)

    def test_handler_url_is_correct(self):
    	correct_url = 'http://smstraffic.ru/superapi/message'
        self.assertEquals(self.handler.API_URL, correct_url)

    def test_prepare_post_data_return_right_data(self):
        request = {'message': 'message', 'phone': self.PHONE}
        expected = {
            'message': request['message'],
            'phone': request['phone'],
            'password': self.AUTH_CRED['password'],
            'login': self.AUTH_CRED['login'],
        }
        self.assertEquals(self.handler.prepare_post_data(**request), expected)
