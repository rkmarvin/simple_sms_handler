#! coding: utf-8
from unittest import TestCase
from sms_handler import base_sms_handler


class BaseTestCaseMixIn(object):
    PHONE = '79149009900'
    OK_RESPONSE = {'status': 'ok', 'phone': PHONE}
    ERROR_RESPONSE = {
        'status': 'error',
        'phone': PHONE,
        'error_code': -3500,
        'error_msg': u'Невозможно отправить сообщение указанному абоненту'
    }
    HANDLER_RESPONSE_OK = {
        'phone': PHONE,
        'status': base_sms_handler.STATUS_OK,
        'message': 'message',
        'gate_response': OK_RESPONSE
    }
    HANDLER_RESPONSE_ERROR = {
        'phone': PHONE,
        'status': base_sms_handler.STATUS_ERROR,
        'message': 'message',
        'gate_response': ERROR_RESPONSE
    }
    AUTH_CRED = {'login': 'login', 'password': 'pws'}


class BaseTestCase(BaseTestCaseMixIn, TestCase):
    pass
