#! coding: utf-8
import json
import responses
from unittest import TestCase

from sms_handler.smsc_handler import SmscHandler
from base_test_case import BaseTestCase


class TestSmscHandler(BaseTestCase):

    def setUp(self):
        self.handler = SmscHandler(self.AUTH_CRED)

    def test_handler_url_is_correct(self):
    	correct_url = 'http://smsc.ru/someapi/message/'
        self.assertEquals(self.handler.API_URL, correct_url)

    def test_prepare_post_data_return_right_data(self):
        request = {'message': 'message', 'phone': self.PHONE}
        expected = {
            'mes': request['message'],
            'phone': request['phone'],
            'pws': self.AUTH_CRED['password'],
            'login': self.AUTH_CRED['login'],
        }
        self.assertEquals(self.handler.prepare_post_data(**request), expected)