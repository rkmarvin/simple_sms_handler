import json
import responses
from unittest import TestCase
from mockito import *

from sms_handler.base_sms_handler import BaseSmsHandler
from sms_handler.base_sms_handler import STATUS_ERROR
from base_test_case import BaseTestCase


class TestBaseSmsHandler(BaseTestCase):

    def setUp(self):
        self.handler = BaseSmsHandler(self.AUTH_CRED)

    def test_handler_url_is_correct(self):
    	correct_url = ''
        self.assertEquals(self.handler.API_URL, correct_url)

    def test_prepare_post_data_raise_not_implemented_error(self):
    	with self.assertRaises(NotImplementedError):
    		self.handler.prepare_post_data()

    @responses.activate
    def test_handler_send_message_and_get_ok(self):
    	when(self.handler).prepare_post_data(any()).thenReturn({})
    	self.handler.API_URL = 'http://test.sms.com/api'
        responses.add(responses.POST, self.handler.API_URL,
                      body = json.dumps(self.OK_RESPONSE),
                      content_type='application/json',
                      status=200)
        resp = self.handler.send('message', self.PHONE)
        self.assertEquals(resp, self.HANDLER_RESPONSE_OK)

    @responses.activate
    def test_handler_send_message_and_get_error(self):
    	when(self.handler).prepare_post_data(any()).thenReturn({})
    	self.handler.API_URL = 'http://test.sms.com/api'
        responses.add(responses.POST, self.handler.API_URL,
                      content_type='application/json',
                      body = json.dumps(self.ERROR_RESPONSE), status=400)
        resp = self.handler.send('message', self.PHONE)
        self.assertEquals(resp, self.HANDLER_RESPONSE_ERROR)

    @responses.activate
    def test_handler_send_message_with_wrong_content_type(self):
        when(self.handler).prepare_post_data(any()).thenReturn({})
        self.handler.API_URL = 'http://test.sms.com/api'
        responses.add(responses.POST, self.handler.API_URL,
                      body = json.dumps(self.OK_RESPONSE),
                      content_type='text/plaint',
                      status=200)
        resp = self.handler.send('message', self.PHONE)
        self.assertEquals(resp['status'], STATUS_ERROR)
        self.assertEquals(resp['gate_response'], {'detail': 'Unexpected content type.'})
